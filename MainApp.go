package main

import (
	"bufio"
	"fmt"
	"github.com/dustin/go-humanize"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

var globalFolderName string = "khmer_found_syllable"
var globalSeparator string = ";"

func main() {
	// runPart01()

	/*
		List files in folder
		https://stackoverflow.com/questions/14668850/list-directory-in-go
	*/
	fmt.Println("============================== LIST ALL FILES IN FOLDER ==============================")
	files, err := ioutil.ReadDir(globalFolderName)
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		fmt.Println(f.Name())
	}

	fmt.Println("============================== CREATE 'CREATED' FILES LIST IN FOLDER ==============================")
	var allCreatedFiles []string
	for _, f := range files {
		var inputFilename string = globalFolderName + "\\" + f.Name()
		if strings.Contains(inputFilename, "CREATED") {
			allCreatedFiles = append(allCreatedFiles, inputFilename)
		}
	}
	fmt.Println("============================== CREATE 'COMPLETED' FILES LIST IN FOLDER ==============================")
	var allCompletedFiles []string
	for _, f := range files {
		var inputFilename string = globalFolderName + "\\" + f.Name()
		if strings.Contains(inputFilename, "CREATED") {
			allCompletedFiles = append(allCompletedFiles, inputFilename)
		}
	}

	/*
		Get diff between Created and Completed files
	*/
	var remainingFiles []string
	for _, tempCreatedFile := range allCreatedFiles {
		var isCreatedFileFound bool = false
		for _, tempCompletedFile := range allCompletedFiles {
			if strings.ReplaceAll(tempCompletedFile, "COMPLETED", "") == strings.ReplaceAll(tempCreatedFile, "CREATED", "") {
				isCreatedFileFound = true
				break
			}
		}
		if isCreatedFileFound == false {
			remainingFiles = append(remainingFiles, tempCreatedFile)
		}
	}

	/*
		Query API
	*/
	// queryApiUsingGoroutine(syllableArray, syllableCounter)
	for _, inputFilename := range remainingFiles {
		fmt.Printf("Working on file '%v'\n", inputFilename)
		// Reading file content
		// https://stackoverflow.com/questions/8757389/reading-a-file-line-by-line-in-go
		var tempFileContent []string
		file, err := os.Open(inputFilename)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			tempFileContent = append(tempFileContent, scanner.Text())
		}

		// Run query
		var savedQueryFilename string = strings.ReplaceAll(inputFilename, "CREATED", "COMPLETED")
		savedQueryFilename = strings.ReplaceAll(savedQueryFilename, globalFolderName, "")
		queryApiImperatively(savedQueryFilename, tempFileContent, int64(len(tempFileContent)))
		fmt.Println("============================================================")
	}
}
func runPart01() {
	var beginningConsonants = []string{
		"ក", "ខ", "គ", "ឃ", "ង",
		"ច", "ឆ", "ជ", "ឈ", "ញ",
		"ដ", "ឋ", "ឌ", "ឍ", "ណ",
		"ត", "ថ", "ទ", "ធ", "ន",
		"ប", "ផ", "ព", "ភ", "ម",
		"យ", "រ", "ល", "វ",
		"ស", "ហ", "ឡ", "អ",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(beginningConsonants)
	*/

	var beginningLiaisons = []string{
		"",
		"្ក", "្ខ", "្គ", "្ឃ", "្ង",
		"្ច", "្ឆ", "្ជ", "្ឈ", "្ញ",
		"្ដ", "្ឋ", "្ឌ", "្ឍ", "្ណ",
		"្ត", "្ថ", "្ទ", "្ធ", "្ន",
		"្ប", "្ផ", "្ព", "្ភ", "្ម",
		"្យ", "្រ", "្ល", "្វ",
		"្ស", "្ហ", "្ឡ", "្អ",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(beginningLiaisons)
	*/

	var vowels = []string{
		"", "ា", "ិ", "ី", "ឹ", "ឺ",
		"ុ", "ូ", "ួ", "ើ", "ឿ", "ៀ",
		"េ", "ែ", "ៃ", "ោ", "ៅ", "ំុ",
		"ំ", "ាំ", "ះ", "ុះ", "េះ", "ោះ",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(vowels)
	*/

	var endingConsonants = []string{
		"",
		"ក", "ខ", "គ", "ឃ", "ង",
		"ច", "ឆ", "ជ", "ឈ", "ញ",
		"ដ", "ឋ", "ឌ", "ឍ", "ណ",
		"ត", "ថ", "ទ", "ធ", "ន",
		"ប", "ផ", "ព", "ភ", "ម",
		"យ", "រ", "ល", "វ",
		"ស", "ហ", "ឡ", "អ",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(endingConsonants)
	*/

	var endingLiaisons = []string{
		"",
		"្ក", "្ខ", "្គ", "្ឃ", "្ង",
		"្ច", "្ឆ", "្ជ", "្ឈ", "្ញ",
		"្ដ", "្ឋ", "្ឌ", "្ឍ", "្ណ",
		"្ត", "្ថ", "្ទ", "្ធ", "្ន",
		"្ប", "្ផ", "្ព", "្ភ", "្ម",
		"្យ", "្រ", "្ល", "្វ",
		"្ស", "្ហ", "្ឡ", "្អ",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(endingLiaisons)
	*/

	var punctuations = []string{
		"", "់",
	}
	/*
		COMMENT BECAUSE OF GITLAB CI LIMITS
		fmt.Println(punctuations)
	*/

	/*
		Create all khmer syllable combinations
	*/
	var syllableArray, syllableCounter = createAllKhmerSyllableCombinations(
		beginningConsonants,
		beginningLiaisons,
		vowels,
		endingConsonants,
		endingLiaisons,
		punctuations)

	/*
		Create 'khmer_found_syllable' folder
	*/
	createGlobalStorageFolder()

	/*
		Backup generated combinations
	*/
	backupGeneratedCombinations(syllableArray, syllableCounter)

	/*
		Split all generated syllables into multiple files
	*/
	var fileContent []string
	var fileCounter int = 0
	for i := 0; i < int(syllableCounter); i++ {
		if (i+1)%50000 != 0 {
			fileContent = append(fileContent, syllableArray[i])
		} else {
			fileCounter++
			var outputFilename = "CREATED_ALL_SYLLABLE_SUBSET_" + strconv.Itoa(fileCounter) + ".txt"
			for j := 0; j < len(fileContent); j++ {
				// https://maxchadwick.xyz/blog/concatenate-a-string-and-an-int-in-go
				writeToFile(outputFilename, fileContent[j])
			}
			/*
				COMMENT BECAUSE OF GITLAB CI LIMITS
				fmt.Printf("File '%v' was created successfully!\n", outputFilename)
			*/
			if fileCounter%100 == 0 {
				fmt.Printf("Number of file created: '%v'\n", fileCounter)
			}

			// Clear array
			fileContent = nil
		}
	}
	if fileContent != nil {
		fileCounter++
		var outputFilename = "CREATED_ALL_SYLLABLE_SUBSET_" + strconv.Itoa(fileCounter) + ".txt"
		for j := 0; j < len(fileContent); j++ {
			// https://maxchadwick.xyz/blog/concatenate-a-string-and-an-int-in-go
			writeToFile(outputFilename, fileContent[j])
		}
		/*
			COMMENT BECAUSE OF GITLAB CI LIMITS
			fmt.Printf("File '%v' was created successfully!\n", outputFilename)
		*/

		// Clear array
		fileContent = nil
	}
	fmt.Printf("Number of subset created: %v\n", fileCounter)
}
func organizeSyllableByCategory() {
	/*
		Create all SyllableCategory combination
	*/
	/*
		var ORConsonantList = []string{"ក", "ខ", "ច", "ឆ", "ដ", "ឋ", "ណ", "ត", "ថ", "ប", "ផ", "ស", "ហ", "ឡ", "អ"}
		var OREConsonantList = []string{"គ", "ឃ", "ង", "ជ", "ឈ"}

		var KEndingConsonantList = []string{""}
		var GNEndingConsonantList = []string{""}
		var CHowelList = []string{""}
		var NGEndingConsonantList = []string{""}
		var TEndingConsonantList = []string{""}
		var NEndingConsonantList = []string{""}
		var BEndingConsonantList = []string{""}
		var MEndingConsonantList = []string{""}
		var YEndingConsonantList = []string{""}
		var REndingConsonantList = []string{""}
		var LEndingConsonantList = []string{""}
		var VEndingConsonantList = []string{""}
		var SEndingConsonantList = []string{""}

		var punctuationList = []string{"", "់"}

		// OR + <Vowel> + K + <Punctuation>
		for i := 0; i < len(ORConsonantList); i++ {
			for j := 0; j < len(vowels); j++ {
				for k := 0; k < len(KEndingConsonantList); k++ {
					for l := 0; l < len(punctuationList); l++ {

					}
				}
			}
		}
	*/
}
func createAllKhmerSyllableCombinations(
	pBeginningConsonants []string,
	pBeginningLiaisons []string,
	pVowels []string,
	pEndingConsonants []string,
	pEndingLiaisons []string,
	pPunctuations []string,
) ([]string, int64) {
	fmt.Println("============================== createAllKhmerSyllableCombinations() ==============================")
	var syllableCounter int64 = 0
	var syllableArray []string
	for _, bc := range pBeginningConsonants {
		for _, bl := range pBeginningLiaisons {
			for _, vw := range pVowels {
				for _, ec := range pEndingConsonants {
					for _, el := range pEndingLiaisons {
						for _, pn := range pPunctuations {
							var tempSyllable = bc + bl + vw + ec + el + pn
							syllableCounter++
							/*
								COMMENT BECAUSE OF GITLAB CI LIMITS
								fmt.Printf("Creating (%v) syllableCounter: %v\n", tempSyllable, humanize.Comma(syllableCounter))
							*/
							syllableArray = append(syllableArray, tempSyllable)
						}
					}
				}
			}
		}
	}
	fmt.Printf(">>>>> Number of syllables generated %v\n", humanize.Comma(syllableCounter)) // 62,257,536
	return syllableArray, syllableCounter
}

func createGlobalStorageFolder() {
	err := os.Mkdir(globalFolderName, 0755)
	if err != nil {
		fmt.Printf("Folder '%v' already exists.\n", globalFolderName)
	} else {
		fmt.Printf("Folder '%v' was created successfully!\n", globalFolderName)
	}
}

func backupGeneratedCombinations(pSyllableArray []string, pSyllableCounter int64) {
	fmt.Println("============================== backupGeneratedCombinations() ==============================")
	var f *os.File
	var err error
	// solution from:
	// https://stackoverflow.com/questions/12518876/how-to-check-if-a-file-exists-in-go
	if _, err = os.Stat(globalFolderName + "/ALL_KHMER_SYLLABLE_COMBINATIONS.txt"); err == nil {
		// path/to/whatever exists
		fmt.Println("File 'ALL_KHMER_SYLLABLE_COMBINATIONS.txt' already exists. Nothing to add.")
	} else if os.IsNotExist(err) {
		// path/to/whatever does *not* exist
		fmt.Println("Creating file 'ALL_KHMER_SYLLABLE_COMBINATIONS.txt'.")
		f, err = os.OpenFile(globalFolderName+"/ALL_KHMER_SYLLABLE_COMBINATIONS.txt", os.O_CREATE|os.O_WRONLY, 0755)
		for i := 0; i < len(pSyllableArray); i++ {
			var tempSyllable string = pSyllableArray[i]
			/*
				COMMENT BECAUSE OF GITLAB CI LIMITS
				var tempIndex, _ = strconv.ParseInt(strconv.Itoa(i+1), 10, 64)
				fmt.Printf("Writing '%v' to file = %v out of %v\n", tempSyllable, humanize.Comma(tempIndex), humanize.Comma(pSyllableCounter))
			*/
			f.WriteString(tempSyllable)
			f.WriteString("\n")
		}
		f.Close()
	} else {
		// Schrodinger: file may or may not exist. See err for details.
		// Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence
		log.Fatal(err)
	}
}

func queryApiUsingGoroutine(pSyllableArray []string, pSyllableCounter int64) {
	fmt.Println("============================== START QUERYING API ==============================")
	var wg sync.WaitGroup
	var workerCount = 0
	var syllableArraySubset []string = nil
	for i := 0; i < len(pSyllableArray); i++ {
		var tempIndex, _ = strconv.ParseInt(strconv.Itoa(i+1), 10, 64)
		fmt.Printf("Working on '%v' = %v out of %v\n", pSyllableArray[i], humanize.Comma(tempIndex), humanize.Comma(pSyllableCounter))
		// Subset of 1 million was too small, so increase to 2.5 million = 25 workers
		if (i+1)%100000 == 0 {
			syllableArraySubset = append(syllableArraySubset, pSyllableArray[i])
			workerCount++
			wg.Add(workerCount)
			go queryEnglishKhmerAPI(syllableArraySubset, &wg, workerCount)
			syllableArraySubset = nil

			// Here we wait for 30 seconds after starting the goroutine
			// This is to try to avoid 'File read error: open /path/to/file: too many open files'
			time.Sleep(30 * time.Second)
		} else {
			syllableArraySubset = append(syllableArraySubset, pSyllableArray[i])
		}

		if i == len(pSyllableArray)-1 {
			workerCount++
			wg.Add(workerCount)
			go queryEnglishKhmerAPI(syllableArraySubset, &wg, workerCount)
		}
	}
	fmt.Printf(">>>>> Number of worker created: '%v'\n", workerCount)
	writeToFile("NUM_WORKER_CREATED.txt", ">>>>> Number of worker created: "+strconv.Itoa(workerCount))
	wg.Wait()
	fmt.Println("============================== END QUERYING API ==============================")
}
func queryApiImperatively(
	pSavedQueryFilename string,
	pSyllableArray []string,
	pSyllableCounter int64,
) {
	fmt.Println("============================== queryApiImperatively() ==============================")
	var outputFileContent []string
	for i := 0; i < len(pSyllableArray); i++ {
		tempCounter, _ := strconv.ParseInt(strconv.Itoa(i+1), 10, 64)
		fmt.Printf("Working on '%v'. #%v of %v\n", pSyllableArray[i], humanize.Comma(tempCounter), humanize.Comma(pSyllableCounter))

		if pSyllableArray[i] == "ERASED" {
			continue
		}

		// Add protection against http.get() error:
		// 		dial tcp 104.177.158.25:80: connectex: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.
		// solution from:
		// https://yourbasic.org/golang/do-while-loop/
		var isQueryErrorEncountered bool = false
		for {
			// Uncomment below for testing
			// var url = "http://english-khmer.com/index.php?gcm=3&gword=ស្រី"
			var url = "http://english-khmer.com/index.php?gcm=3&gword=" + pSyllableArray[i]

			resp, err := http.Get(url)
			if err != nil {
				isQueryErrorEncountered = true
				fmt.Printf("Error encountered with '%v'. Retrying in 5 seconds...\n", pSyllableArray[i])
				time.Sleep(5 * time.Second)
			} else {
				if isQueryErrorEncountered == true {
					fmt.Printf("'%v' was resolved.\n", pSyllableArray[i])
					isQueryErrorEncountered = false
				}

				body, _ := io.ReadAll(resp.Body)
				var startIndex = strings.Index(string(body), "<font color=\"blue\" size=3>") + 26
				var endIndex = strings.Index(string(body), "</font></td>")
				if endIndex > startIndex {
					outputFileContent = append(outputFileContent, string(body[startIndex:endIndex])+globalSeparator+"FOUND")
				} else {

					outputFileContent = append(outputFileContent, pSyllableArray[i]+globalSeparator+"NOT_FOUND")
				}

				// to protect against:
				// 		possible resource leak 'defer' is called in a for loop
				// when using:
				// 		defer resp.Body.Close()
				resp.Body.Close()
				break
			}
		}
	}

	// Write to file only when every query has been processed
	for i := 0; i < len(outputFileContent); i++ {
		writeToFile(pSavedQueryFilename, outputFileContent[i])
	}
	fmt.Printf("File '%v' was created successfully!\n", pSavedQueryFilename)
}

// solution from:
// https://stackoverflow.com/questions/1821811/how-to-read-write-from-to-a-file-using-go
// https://gobyexample.com/writing-files
func writeToFile(pFilename string, pLine string) {
	// Create filename
	// https://www.golangprograms.com/get-current-date-and-time-in-various-format-in-golang.html
	// currentTime := time.Now()
	// var datePrefix = currentTime.Format("2006-01-02_15-04-05-000000000") // "YYYY-MM-DD
	// var filename string = "khmer_found_syllable/" + datePrefix + "_output.txt"
	var filename string = globalFolderName + "/" + pFilename

	// Append to file
	// https://yourbasic.org/golang/append-to-file/
	fo, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		panic(err)
	}

	// close fo on exit and check for its returned error
	defer fo.Close()

	w := bufio.NewWriter(fo)
	w.WriteString(pLine)
	w.WriteString("\n")
	w.Flush()
}

// solution from:
// https://pkg.go.dev/net/http
// https://blog.logrocket.com/making-http-requests-in-go/
func queryEnglishKhmerAPI(pSyllables []string, pWorker *sync.WaitGroup, pWorkerId int) {
	fmt.Println("============================== QUERY API ==============================")
	fmt.Printf("Worker %v: Started\n", pWorkerId)

	// Here we try to delete all syllable that was already queried from a previous run
	// This allows us to run multiple times the program and start from where we left out
	// solution from:
	// https://zetcode.com/golang/readfile/
	var filename = "WORKER_" + strconv.Itoa(pWorkerId) + "_START.txt"
	fileReader, err := os.Create(globalFolderName + "/" + filename)
	if err != nil {
		fmt.Printf("File '%v' already exists.\n", filename)
	}
	defer fileReader.Close()
	//// We remove each syllable read from file on 'pSyllables'
	scanner := bufio.NewScanner(fileReader)
	for scanner.Scan() {
		var lineArray []string = strings.Split(scanner.Text(), ",")
		for i := 0; i < len(lineArray); i++ {
			fmt.Printf("\tLine[%v]: %v", i, lineArray[i])
		}
		for i := 0; i < len(pSyllables); i++ {
			if strings.ToUpper(lineArray[0]) == strings.ToUpper(pSyllables[i]) {
				pSyllables[i] = ""
				break
			}
		}
	}
	//// Create a new array 'validatedSyllables' with the remaining syllables from 'pSyllables'
	var validatedSyllables []string = nil
	for i := 0; i < len(pSyllables); i++ {
		validatedSyllables = append(validatedSyllables, pSyllables[i])
	}

	for i := 0; i < len(validatedSyllables); i++ {
		var syllableToQuery string = validatedSyllables[i]
		// Add protection against http.get() error:
		// 		dial tcp 104.177.158.25:80: connectex: A connection attempt failed because the connected party did not properly respond after a period of time, or established connection failed because connected host has failed to respond.
		// solution from:
		// https://yourbasic.org/golang/do-while-loop/
		var isQueryErrorEncountered bool = false
		for {
			// Uncomment below for testing
			// var url = "http://english-khmer.com/index.php?gcm=3&gword=ស្រី"
			var url = "http://english-khmer.com/index.php?gcm=3&gword=" + syllableToQuery

			resp, err := http.Get(url)
			if err != nil {
				isQueryErrorEncountered = true
				fmt.Printf("Worker #%v: Error encountered with '%v'. Retrying in 5 seconds...\n", pWorkerId, syllableToQuery)
				time.Sleep(5 * time.Second)
			} else {
				if isQueryErrorEncountered == true {
					fmt.Printf("Worker #%v: Error encountered with '%v' was resolved.\n", pWorkerId, syllableToQuery)
					isQueryErrorEncountered = false
				}

				body, _ := io.ReadAll(resp.Body)
				var startIndex = strings.Index(string(body), "<font color=\"blue\" size=3>") + 26
				var endIndex = strings.Index(string(body), "</font></td>")
				if endIndex > startIndex {
					writeToFile(filename, string(body[startIndex:endIndex])+", FOUND")
				} else {
					writeToFile(filename, syllableToQuery+", NOT_FOUND")
				}

				// to protect against:
				// 		possible resource leak 'defer' is called in a for loop
				// when using:
				// 		defer resp.Body.Close()
				resp.Body.Close()
				break
			}
		}
	}
	fmt.Printf("Worker %v: Finished\n", pWorkerId)
	writeToFile("WORKER_"+strconv.Itoa(pWorkerId)+"_END.txt", "")
	defer pWorker.Done()
}

func queryGoogleTranslateAPI(pSyllables []string) {
	fmt.Println("============================== QUERY API ==============================")
	for _, syllable := range pSyllables {
		var url = "https://translate.google.com/?hl=en&sl=km&tl=en&text=" + syllable + "&op=translate"
		resp, err := http.Get(url)
		if err != nil {
			log.Fatalln(err)
		}
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		var startIndex = strings.Index(string(body), "<font color=\"blue\" size=3>") + 26
		var endIndex = strings.Index(string(body), "</font></td>")
		if endIndex > startIndex {
			writeToFile("", string(body[startIndex:endIndex]))
		}
	}
}

// solution from:
// https://goinbigdata.com/golang-wait-for-all-goroutines-to-finish/
// https://stackoverflow.com/questions/18207772/how-to-wait-for-all-goroutines-to-finish-without-using-time-sleep
func worker(wg *sync.WaitGroup, id int) {
	defer wg.Done()
	fmt.Printf("Worker %v: Started\n", id)
	fmt.Printf("Worker %v: Finished\n", id)
}
